# SMSPP Hydro Units

The main aim of this project was to create new “HydroUnitBlock” blocks for SMS++ for purposes of testing. These new blocks would be stored in NetCDF4 files, since that is the file type that SMS++ uses. Although not necessary, the more diverse and varied the data sets would be the better (especially with regards to size and number of reservoirs and arcs).

In the end a total of 7 new hydro blocks were created based on cascading hydro systems in Norway. Additionally, code was created in Jupyter Notebook to allow additional hydro blocks to be created more easily from Excel spreadsheets. The code can also read blocks (any type) from netcdf files and could be used to combine blocks from different files.

Please find the "Report" file (either Word document or pdf) and read it carefully to get more information and understand the project better. The "File Formats" file and "Solver Descriptions" files might be useful to you as well if you aren't too familiar with the data formats and solvers in SMS++. The code is contained in the Ipython notebook "Notebook Final.ipynb".
